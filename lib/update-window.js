"use strict"

const {app, BrowserWindow, dialog, shell, ipcMain, systemPreferences} = require("electron")
const { spawn } = require('child_process')
const crypto = require("crypto")
const fs = require("fs")
const settings = require("settings-store")
const path = require("path")
const url = require("url")

const getSHA256 = function(file) {
    return new Promise((resolve, reject) => {
        const hash = crypto.createHash("sha256")
        const stream = fs.createReadStream(file)
        stream.on("data", data => hash.update(data))
        stream.on("end", () => resolve(hash.digest("hex")))
        stream.on("error", error => reject(errpr))
    })
}

const copyFile = function(source, target) {
    return new Promise((resolve, reject) => {
        if (fs.copyFile) {
            fs.copyFile(source, target, error => {
                if (error) {
                    reject(`Could not copy "${source}" to ${target}: ${error}`)
                } else {
                    resolve()
                }
            })
        } else {
            const writeStream = fs.createWriteStream(target)
                .on("error", error => reject(`Could not write file ${target}: ${error}`))
            const readStream = fs.createReadStream(source)
                .on("error", error => reject(`Could not read file ${source}: ${error}`))
            readStream.pipe(writeStream)
                .on("close", () => resolve())
                .on("error", error => reject(`Could not copy "${source}" to ${target}: ${error}`))
        }
    })
}

const openFile = function(path) {
    if (this.opts.startUpdateAsProcess) {
        const process = spawn(path, {detached: true})
        process.unref()
    } else {
        shell.openItem(path)
    }
}

const installUpdate = function() {
    fs.chmodSync(this.downloadFile, "755")
    if (this.opts.saveUpdateFile) {
        const dir = this.opts.saveUpdateFileDirectory ||
            settings.value("dblsqd.saveUpdateFileDirectory", "")
        const urlPathnameSegments = url
            .parse(this.release.download.url)
            .pathname
            .split("/")
        const filename = urlPathnameSegments[urlPathnameSegments.length - 1]
        const defaultPath = path.join(dir, filename)
        const savePath = dialog.showSaveDialog(this.win, {defaultPath: defaultPath})
        if (!savePath) throw "No target file selected."
        settings.setValue("dblsqd.saveUpdateFileDirectory", path.dirname(savePath))
        copyFile(this.downloadFile, savePath).then(() => {
            fs.chmodSync(savePath, "755")
            openFile.apply(this, [savePath])
            app.quit()
        }).catch(error => {
            onDownloadFailed(error)
        })
    } else {
        openFile.apply(this, [this.downloadFile])
        app.quit()
    }
}

const onDownloadFinished = function(file) {
    getSHA256(file).then(sha256 => {
        if (sha256 !== this.release.download.sha256) {
            return onDownloadFailed.apply(this, ["Could not verify download integrity."])
        } else {
            this.downloadFinished = true
            this.downloadFile = file
            settings.setValue("dblsqd.downloadFile", file)
            settings.setValue("dblsqd.downloadFileVersion", this.release.version)
        }

        if (this.accepted === true) {
            installUpdate.apply(this)
        } else {
            try {
                this.win.webContents.send("disable-buttons", false)
            } catch (e) {}
        }
    })
    .catch(error => onDownloadFailed.apply(this, [error]))
}

const onDownloadFailed = function(error) {
    dialog.showErrorBox("Downloading update failed", error || "Unknown error")
    try {
        this.win.close()
    } catch (e) {}
}

const deleteDownloadFile = function() {
    const downloadFile = settings.value("dblsqd.downloadFile")
    if (!downloadFile) return
    try {
        fs.unlinkSync(downloadFile)
        settings.delete("dblsqd.downloadFile")
    } catch (e) {}
}

const startDownload = function() {
    deleteDownloadFile()
    this.downloading = true
    const downloadCallback = (bytesReceived, bytesTotal) => {
        try {
            this.win.webContents.send("download-progress", bytesReceived, bytesTotal)
            this.win.webContents.send("disable-buttons", true)
        } catch (e) {}
    }
    this.feed.downloadRelease(this.release.version, downloadCallback)
        .then(file => {
            this.downloading = false
            onDownloadFinished.apply(this, [file])
        })
        .catch(error => {
            this.downloading = false
            onDownloadFailed.apply(this, [error])
        })
}

const onAccept = function() {
    this.accepted = true
    if (this.downloadFinished) {
        installUpdate.apply(this)
    } else {
        startDownload.apply(this)
    }
}

const onCancel = function() {
    this.win.close()
}

const onSkip = function() {
    settings.setValue("dblsqd.skipRelease", this.release.version)
    this.win.close()
}

const onToggleAutoDownload = enable => {
    settings.setValue("dblsqd.autoDownload", enable)
}

const setupUi = function() {
    const win = this.win
    if (!win) return

    const updateVersion = this.release && this.release.version
    const releases = this.mode === "update" ? this.updates : this.releases
    ipcMain.once("dblsqd-ready", () => {
        win.webContents.send("data-available", {
            appName: app.getName(),
            currentVersion: app.getVersion(),
            updateVersion: updateVersion,
            releases: releases,
            autoDownload: settings.value("dblsqd.autoDownload"),
            downloadFinished: this.downloadFinished,
            icon: this.opts.icon,
            os: process.platform
        })
        if (this.downloading) {
            win.webContents.send("disable-buttons", true)
        }
    })
    if (!this.loaded) {
        win.loadURL(`file://${__dirname}/../content/loading.html`)
        win.setContentSize(600, 125)
    } else if (this.mode === "update" && this.updates && this.updates.length > 0) {
        win.loadURL(`file://${__dirname}/../content/index.html`)
        win.setContentSize(600, 350)
    } else if (this.mode === "changelog" && this.releases && this.releases.length > 0) {
        win.loadURL(`file://${__dirname}/../content/changelog.html`)
        win.setContentSize(600, 350)
    } else {
        win.loadURL(`file://${__dirname}/../content/no-updates.html`)
        win.setContentSize(600, 175)
    }
}

/**
 * Drop-in UI for auto-updates in Electron apps.
 * @class UpdateWindow
 * @param {Feed} feed 
 * @param {Object} [opts]
 * @param {string} opts.icon - Path to an icon.
 * @param {boolean} opts.saveUpdateFile - If true, a save dialog is shown for the user to pick a permanent path for the downloaded file.
 * @param {boolean} opts.saveUpdateFileDirectory - Initial directory for the save dialog. Defaults to previously used path if available.
 * @param {boolean} opts.startUpdateAsProcess - Launches the downloaded files as an executable in a new process instead of using electron.shell.openExternal. Required for executables on most Linux desktops.
 * @param {"update" | "will-quit" | "manual"} opts.showOn - When to show the UpdateWindow.
 * @param {"update" | "changelog"} opts.mode - What to display.
 * @param {"electron.autoUpdater"} opts.integration - Automatically integrate with an auto-update mechanism.
 * @param {BrowserWindow} opts.parent - Parent BrowserWindow for modal UpdateWindow.
 */
function UpdateWindow(feed, opts) {
    opts = this.opts = opts || {}
    this.feed = feed

    feed.load().then(() => {
        const updates = this.updates = feed.getUpdates(app.getVersion())
        const releases = this.releases = feed.getUpdates("0.0.0", app.getVersion())
        const release = this.release = updates[0]
        const showOn = this.showOn = opts.showOn || "update"
        const mode = this.mode = opts.mode || "update"
        this.loaded = true
        setupUi.apply(this)

        const skipRelease = settings.value("dblsqd.skipRelease") === release.version
        const autoDownload = settings.value("dblsqd.autoDownload", false) === true
        const downloadFile = settings.value("dblsqd.downloadFileVersion") === release.version &&
                             settings.value("dblsqd.downloadFile")

        //Delete previously downloaded updates
        if (updates.length == 0 || skipRelease) deleteDownloadFile()

        //Re-use previous download or start automatic download
        if (mode === "update") {
            if (downloadFile && fs.existsSync(downloadFile)) {
                this.downloadFile = downloadFile
                this.downloadFinished = true
            } else if (autoDownload && updates.length > 0) {
                startDownload.apply(this)
            }
        }

        //Don’t automatically show window when there are no updates
        if (mode === "update" && (skipRelease || updates.length === 0)) return

        switch (showOn) {
            case "update":
                this.show()
                break
            case "before-quit":
            case "will-quit":
                app.once("will-quit", e => {
                    e.preventDefault()
                    this.show()
                })
                break
        }
    }).catch(e => {
        this.loaded = true
        setupUi.apply(this)
    })
}

/**
 * Shows the UpdateWindow.
 * @memberof UpdateWindow#
 * @method show
 * @param {BrowserWindow} [parent] - Set the parent of the UpdateWindow.
 * @returns {BrowserWindow} - Returns the newly created BrowserWindow.
 * 
 */
UpdateWindow.prototype.show = function(parent) {
    if (this.win) {
        this.win.show()
        return this.win
    }
    parent = typeof parent !== "undefined" ? parent : this.opts.parent
    const windowColor = systemPreferences.getColor ?
        systemPreferences.getColor("window") :
        "#f7f7f7"
    const win = this.win = new BrowserWindow({
        width: 600,
        height: 350,
        useContentSize: true,
        background: windowColor,
        parent: parent,
        modal: true,
        show: false,
    })
    win.setMenu(null)

    setupUi.apply(this)
    win.once("ready-to-show", () => win.show())

    ipcMain.on("dblsqd-accept", () => onAccept.apply(this))
    ipcMain.on("dblsqd-cancel", () => onCancel.apply(this))
    ipcMain.on("dblsqd-skip", () => onSkip.apply(this))
    ipcMain.on("dblsqd-toggle-auto-download", (sender, enable) => {
        onToggleAutoDownload.apply(this, [enable])
    })

    return win
}

module.exports = UpdateWindow